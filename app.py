import os
import json
import urllib.parse as urlparse 
from collections import defaultdict
from youtube_title_parser.core import YoutubeTitleParser
from transliterate import translit

import youtube_dl
from mutagen.mp3 import MP3
from flask import Flask, render_template, request, send_from_directory, redirect, url_for, flash
from flask_sqlalchemy import SQLAlchemy
from flask_whooshee import Whooshee

basepath = os.path.dirname(os.path.abspath(__file__))

youtube_prefixes = [
    "https://www.youtube.com/watch",
    "https://m.youtube.com/watch",
]

app = Flask(__name__)

db_path = os.path.join(basepath, 'app.db')
search_index_path = os.path.join(basepath, 'search_index')
app.config['SECRET_KEY'] = "not_secure"
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(db_path)
app.config['WHOOSHEE_DIR'] = search_index_path
db = SQLAlchemy(app)
whooshee = Whooshee(app)

songs_tags = db.Table('songs_labels',
        db.Column('song_id', db.Integer(), db.ForeignKey('song.id')),
        db.Column('label_id', db.Integer(), db.ForeignKey('label.id')))


@whooshee.register_model('title', 'indexing_title')
class Song(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    
    title = db.Column(db.Unicode)
    indexing_title = db.Column(db.Unicode)
    filename = db.Column(db.Unicode)
    duration = db.Column(db.Integer)
    
    default_start = db.Column(db.Integer)
    default_end = db.Column(db.Integer)
    
    labels = db.relationship("Label", secondary=songs_tags)


@whooshee.register_model('value')
class Label(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    
    key = db.Column(db.Unicode)
    value = db.Column(db.Unicode)
    
    songs = db.relationship("Song", secondary=songs_tags)


if not os.path.isfile(db_path):
    with app.app_context():
        db.create_all()
else:
    if not os.path.isfile(search_index_path):
        whooshee.reindex()

@app.route("/")
def index():
    label_keys = []
    labels = Label.query.group_by(Label.key).all()
    for label in labels:
        label_keys.append(label.key)
    return render_template("index.html", label_keys=label_keys)

@app.route("/all-songs")
def all_songs():
    all_songs = Song.query.all()
    return render_template("all_songs.html", all_songs=all_songs)


@app.route("/listen/<int:song_id>")
def listen(song_id):
    song = Song.query.filter_by(id=song_id).first()
    if not song:
        flash('No song with id {}'.format(song_id))
        return redirect(url_for('index'))
    media_st = int(request.args.get("s", song.default_start or 0))
    media_st = max(media_st, 0)
    media_duration = int(song.duration)
    media_et = int(request.args.get("e", song.default_end or media_duration))
    media_et = min(media_et, media_duration)
    return render_template("listen.html",
                            song=song,
                            media_fname=song.filename,
                            media_st=media_st,
                            media_et=media_et,
                            media_length=media_duration)


@app.route("/songs/<int:song_id>/edit", methods=["post"])
def edit_song(song_id):
    song = Song.query.filter_by(id=song_id).first()
    if not song:
        flash('No song with id {}'.format(song_id))
        return redirect(request.referer)
    song.title = request.form.get('title')
    song.indexing_title = request.form.get('indexing_title')
    song.default_start = request.form.get('default_start')
    song.default_end = request.form.get('default_end')
    db.session.add(song)
    db.session.commit()
    return redirect(request.referrer)


@app.route("/labels")
def labels():
    label_key = request.args.get('k')
    label_value = request.args.get('v')
    all_songs, label_keys, label_values = [],[],[]
    if label_key and label_value:
        labels = Label.query.filter_by(key=label_key, value=label_value).all()
        all_songs = []
        for label in labels:
            for song in label.songs:
                all_songs.append(song)
    elif label_key:
        labels = Label.query.filter_by(key=label_key).group_by(Label.value).all()
        for label in labels:
            label_values.append(label.value)
    else:
        labels = Label.query.group_by(Label.key).all()
        for label in labels:
            label_keys.append(label.key)
    return render_template("label.html",
                           label_key=label_key,
                           label_value=label_value,
                           all_songs=all_songs, 
                           label_keys=label_keys, 
                           label_values=label_values)


@app.route("/song/<int:song_id>/labels/add", methods=["post"])
def add_label_to_song(song_id):
    song = Song.query.filter_by(id=song_id).first()
    if not song:
        flash('No song with id {}'.format(song_id))
        return redirect(request.referrer)
    label_key = request.form.get('key')
    label_value = request.form.get('value')
    label = Label(key=label_key, value=label_value)
    song.labels.append(label)
    db.session.add(song)
    db.session.commit()
    return redirect(url_for('listen', song_id=song.id))


@app.route("/labels/<int:label_id>/edit", methods=["post"])
def edit_label(label_id):
    label = Label.query.filter_by(id=label_id).first()
    if not label:
        flash('No label with id {}'.format(label_id))
        return redirect(request.referer)
    label.key = request.form.get('key')
    label.value = request.form.get('value')
    db.session.add(label)
    db.session.commit()
    return redirect(request.referrer)
    
@app.route("/labels/<int:label_id>/delete", methods=["post"])
def delete_label(label_id):
    label = Label.query.filter_by(id=label_id).first()
    if not label:
        flash('No label with id {}'.format(label_id))
        return redirect(request.referer)
    db.session.delete(label)
    db.session.commit()
    return redirect(request.referrer)


@app.route("/search")
def search():
    q = request.args.get("q")
    if not q:
        return redirect(request.referrer)
    songs = Song.query.whooshee_search(q).all()
    labels = defaultdict(lambda: dict(key='', value='', songs=[]))
    match_labels = Label.query.whooshee_search(q).all()
    for ml in match_labels:
        labels[ml.key]['key'] = ml.key
        labels[ml.key]['value'] = ml.value
        labels[ml.key]['songs'] += ml.songs
    q = q.strip()
    video_id = None
    for prefix in youtube_prefixes:
        if q.startswith(prefix):
            parsed = urlparse.urlparse(q)
            video_id = urlparse.parse_qs(parsed.query).get('v', [None])[0]
            break
    labels_by_youtube_video_id = Label.query.filter_by(key="youtube_video_id", value=video_id)
    youtube_songs = []
    for label in labels_by_youtube_video_id:
        for song in label.songs:
            youtube_songs.append(song)
    if len(youtube_songs) == 1:
        return redirect(url_for('listen', song_id=youtube_songs[0].id))
    return render_template('search_results.html',
                            download_youtube_video=video_id,
                            youtube_songs=youtube_songs,
                            songs=songs,
                            labels=labels,
                            q=q,
                            )

class YdLogger(object):
    def __init__(self):
        self.info_json = {}
    
    def warning(self, msg):
        pass
    
    def error(self, msg):
        pass
        
    def debug(self, msg):
        try:
            self.info_json = json.loads(msg)
        except:
            print(msg)

@app.route("/download")
def download():
    q = request.args.get("q")
    next_song_id = Song.query.count() + 1
    media_fpath = os.path.join(basepath, "media", str(next_song_id))
    yd_logger = YdLogger()
    ydl_opts = {
        'outtmpl': "{}.%(ext)s".format(media_fpath),
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
        'forcejson': True,
        'logger': yd_logger,
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download(['http://www.youtube.com/watch?v={}'.format(q)])
    song_title = yd_logger.info_json['title']
    song_duration = yd_logger.info_json['duration']
    # media_meta = MP3("{}.mp3".format(media_fpath))
    # song_length = int(media_meta.info.length)
    song = Song(filename="{}.mp3".format(next_song_id), duration=song_duration, title=song_title)
    youtube_label = Label(key="youtube_video_id", value=q)
    song.labels.append(youtube_label)
    
    try:
        yp = YoutubeTitleParser(song_title)
        if yp.artist_name:
            artist_label = Label(key="artist", value=yp.artist_name)
            song.labels.append(artist_label)
    except:
        pass
    
    try:
        indexing_title = translit(song_title, 'bg', reversed=True)
        if indexed_title:
            song.indexing_title = indexing_title
    except Exception as e:
        pass
    
    db.session.add(song)
    db.session.commit()
    return redirect(url_for("listen", song_id=song.id))


@app.route("/media/<path:filepath>")
def get_media(filepath):
    return send_from_directory("media", filepath)

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1 and sys.argv[1] == 'prod':
        app.run(host='0.0.0.0', port=5001)
    else:
        app.run(debug=True)
